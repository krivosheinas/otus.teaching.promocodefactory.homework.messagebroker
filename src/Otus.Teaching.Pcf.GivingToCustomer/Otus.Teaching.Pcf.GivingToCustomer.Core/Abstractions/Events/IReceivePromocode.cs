﻿using System;

namespace Otus.Teaching.Pcf.Events
{
    public interface IReceivePromocode
    {
        Guid PromoCodeId { get; }
        string PromoCode { get; }
        string ServiceInfo { get; }
        string BeginDate { get; }
        string EndDate { get; }
        Guid PartnerId { get; }
        Guid? PartnerManagerId { get; }
        Guid PreferenceId { get; }
    }
}
