﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Events;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.EventConsumers
{
    public class UpdateAppliedPromocodesEventConsumer : IConsumer<IReceivePromocode>
    {
        private readonly IEmployeeService _employeeService;
        private readonly ILogger<UpdateAppliedPromocodesEventConsumer> _logger;

        public UpdateAppliedPromocodesEventConsumer(
            IEmployeeService employeeService,
            ILogger<UpdateAppliedPromocodesEventConsumer> logger)
        {
            _employeeService = employeeService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IReceivePromocode> context)
        {
            if (!context.Message.PartnerManagerId.HasValue)
            {
                _logger.LogError($"PartnerManagerId не содержит значения");
                return;
            }

            await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }
}
